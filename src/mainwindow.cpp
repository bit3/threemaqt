#include "mainwindow.h"
#include "singletonwebenginepage.h"
#include <QApplication>
#include <QAction>
#include <QSettings>
#include <QWebEngineProfile>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      notificationsTitleRegExp("^\\([1-9]\\d*\\).*"),
      trayIconRead(":/images/threema_badge_white_dots.png"),
      trayIconUnread(":/images/threema_badge_green_dots.png")
{
    setWindowTitle("ThreemaQT");
    setWindowIcon(QIcon(":/images/threema-app-icon_neutral.svg"));
    setMinimumWidth(800);
    setMinimumHeight(600);
    readSettings();

    createActions();
    createStatusBar();
    createTrayIcon();
    createWebEngine();

    // Connect signals and slots
    connect(webEngine, &QWebEngineView::titleChanged,
            this, &MainWindow::handleWebViewTitleChanged);
    connect(webEngine, &QWebEngineView::loadStarted,
            this, &MainWindow::handleLoadStarted);
    connect(webEngine, &QWebEngineView::loadProgress,
            this, &MainWindow::handleLoadProgress);
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested,
            this, &MainWindow::handleDownloadRequested);
}

void MainWindow::readSettings()
{
    QSettings settings("bit3.io", "ThreemaQT");
    restoreGeometry(settings.value("main/geometry").toByteArray());
    restoreState(settings.value("main/windowState").toByteArray());
}

void MainWindow::showAbout()
{
    QString applicationVersion = QCoreApplication::applicationVersion();
    QMessageBox about(this);
    about.setWindowTitle(tr("ThreemaQT"));
    about.setIconPixmap(QPixmap(":/images/threema-app-icon_neutral_64px.png"));
    about.setTextFormat(Qt::TextFormat::RichText);
    about.setText("Unofficial Threema Web Desktop Client.<br>"
                     "<a href=\"https://gitlab.com/bit3/threemaqt\">https://gitlab.com/bit3/threemaqt</a><br>"
                     "<br>"
                     "Version: " + applicationVersion + "<br>"
                     "License: MIT<br>"
                     "<br>"
                     "Attribution: Icon by Threema<br>"
                     "<a href=\"https://threema.ch/en/press\">https://threema.ch/en/press</a>");
    about.addButton(QMessageBox::StandardButton::Ok);
    about.exec();
}

void MainWindow::showAboutQt()
{
    QMessageBox::aboutQt(this, tr("ThreemaQT"));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings("bit3.io", "ThreemaQT");
    settings.setValue("main/geometry", saveGeometry());
    settings.setValue("main/windowState", saveState());
    QMainWindow::closeEvent(event);
}

void MainWindow::createActions()
{
    reloadAction = new QAction(tr("Re&load"), this);
    reloadAction->setShortcut(Qt::Key_F5);
    connect(reloadAction, &QAction::triggered, this, &MainWindow::doReload);
    addAction(reloadAction);

    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);
    addAction(minimizeAction);

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);
    addAction(maximizeAction);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);
    addAction(restoreAction);

    aboutAction = new QAction(tr("About"), this);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::showAbout);

    aboutAction = new QAction(tr("About"), this);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::showAbout);

    aboutQtAction = new QAction(tr("About QT"), this);
    connect(aboutQtAction, &QAction::triggered, this, &MainWindow::showAboutQt);

    quitAction = new QAction(tr("&Quit"), this);
    quitAction->setShortcut(QKeySequence(Qt::Modifier::CTRL + Qt::Key_Q));
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
    addAction(quitAction);
}

void MainWindow::createStatusBar()
{
    QStatusBar *statusBar = new QStatusBar(this);
    setStatusBar(statusBar);
    statusBar->hide();
    this->statusBar = statusBar;

    QProgressBar *progressBar = new QProgressBar(this->statusBar);
    statusBar->addWidget(progressBar, 1);
    progressBar->setTextVisible(false);
    progressBar->hide();
    this->progressBar = progressBar;
}

void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(reloadAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(aboutAction);
    trayIconMenu->addAction(aboutQtAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(trayIconRead, this);
    trayIcon->setContextMenu(trayIconMenu);

    trayIcon->show();

    connect(trayIcon, &QSystemTrayIcon::messageClicked,
            this, &MainWindow::messageClicked);
    connect(trayIcon, &QSystemTrayIcon::activated,
            this, &MainWindow::iconActivated);
}

void MainWindow::createWebEngine()
{
    QSizePolicy widgetSize;
    widgetSize.setHorizontalPolicy(QSizePolicy::Expanding);
    widgetSize.setVerticalPolicy(QSizePolicy::Expanding);
    widgetSize.setHorizontalStretch(1);
    widgetSize.setVerticalStretch(1);

    QWebEngineView *webEngine = new QWebEngineView(this);
    setCentralWidget(webEngine);
    webEngine->setSizePolicy(widgetSize);
    webEngine->show();
    this->webEngine = webEngine;

    QWebEnginePage *page = new SingletonWebEnginePage(webEngine);
    webEngine->setPage(page);
    page->setUrl(QUrl("https://web.threema.ch/"));
}

void MainWindow::handleWebViewTitleChanged(QString title) {
    setWindowTitle(title);

    if (notificationsTitleRegExp.exactMatch(title))
    {
        trayIcon->setIcon(trayIconUnread);
    }
    else
    {
        trayIcon->setIcon(trayIconRead);
    }
}

void MainWindow::handleLoadStarted()
{
    statusBar->show();
    progressBar->show();
}

void MainWindow::handleLoadProgress(int progress)
{
    if (progress >= 100)
    {
        progressBar->hide();
        statusBar->hide();
    }
    else
    {
        progressBar->setValue(progress);
    }
}

void MainWindow::handleDownloadRequested(QWebEngineDownloadItem *download)
{
    QFileDialog dialog;
    dialog.setAcceptMode(QFileDialog::AcceptMode::AcceptSave);
    dialog.setFileMode(QFileDialog::FileMode::AnyFile);
    dialog.selectFile(download->suggestedFileName());

    if (dialog.exec() && dialog.selectedFiles().size() > 0)
    {
        download->setDownloadDirectory(dialog.directory().absolutePath());
        download->setDownloadFileName(dialog.selectedFiles().at(0));
        download->accept();
    }
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (isVisible()) {
        hide();
    } else {
        showNormal();
    }
}

void MainWindow::messageClicked()
{
    if (isVisible()) {
        hide();
    } else {
        showNormal();
    }
}

void MainWindow::doReload()
{
    this->webEngine->triggerPageAction(QWebEnginePage::ReloadAndBypassCache, false);
}
