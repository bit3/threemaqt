= ThreemaQT

Unofficial Threema Web Desktop Client.

[IMPORTANT]
====
*Out of maintenance*

There is now an official client that is also offered for Linux.
https://threema.ch/en/blog/posts/threema-for-desktop
====

image::images/screenshot-main.png[Main Window]

Attribution: Icon by Threema https://threema.ch/en/press

== Flatpak config

The flatpak config file can be found in the link:https://github.com/flathub/io.bit3.ThreemaQT/blob/master/io.bit3.ThreemaQT.yml[flathub/io.bit3.ThreemaQT] repository.

== Tweaks

=== Allow uploading files from everywhere in /home/user

Per default the app is only allowed to access the "downloads" folder, to make it possible to download files and upload from "downloads" folder.
That may be too restrictive for most. To grant access to your home directory, override filesystem access with:

----
flatpak override --filesystem=home io.bit3.ThreemaQT
----

== Local development

=== Installing dependencies

It is recommend to use the *flatpak user mode* installation method.
In this mode the packages are installed into user space, without affecting the system.

==== flatpak user mode

[source,bash]
----
# add flathub remote
$ flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# installing required packages
$ flatpak install --user --assumeyes flathub org.kde.Platform//5.15 org.kde.Sdk//5.15 io.qt.qtwebengine.BaseApp//5.15
----

==== flatpak system mode

[source,bash]
----
# add flathub remote
$ sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# installing required packages
$ sudo flatpak install --assumeyes flathub org.kde.Platform//5.15 org.kde.Sdk//5.15 io.qt.qtwebengine.BaseApp//5.15
----

==== qt5 packages

For local code completion / assistance.
Not needed for compiling!

[source,bash]
----
$ sudo dnf install qt5-qtwebengine-devel
----

NOTE: In the Clion IDE it is necessary to define the `CMAKE_PREFIX_PATH` in the _CMake options_
      (_Settings - Build, Execution, Development - CMake_), e.g. `-DCMAKE_PREFIX_PATH=/usr/lib64/qt5` on fedora.
(link:https://www.jetbrains.com/help/clion/qt-tutorial.html#configure-cmakelists[ref])

=== Building and running the application

[source,bash]
----
$ cd /path/to/threemaqt
# build the app
$ flatpak-builder --force-clean build-dir io.bit3.ThreemaQT.yml
# run the app
$ flatpak-builder --run build-dir io.bit3.ThreemaQT.yml ThreemaQT
----
